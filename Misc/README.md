# Projekt Anforderungen

## Paper

Eine Latex Vorlage angelehnt an das Layout eines Statistik Journals ist im Ordner Templates zu finden.

Es sei verpflichtend diese Vorlage zur Anfertigung der Seminararbeit zu nutzen.

Das einzureichende Paper soll ~15 Seiten Umfang aufweisen. Hierbei sei 2 die maximale Anzahl an durch Grafiken genutzte Seiten. 

Wenn möglich Computer Code nicht direkt im Text nutzen, versuchen zusammenhänge durch Pseudo-Code darzustellen. Falls nicht anders möglich einbindung von Code basierend auf Knittr. 

Inhaltlich sei eine sich an den Daten und der Methode orientierende Arbeit abzugeben.

Die folgende Struktur sei als Vorschlag zu interpretieren und ist nicht verbindlich. Bei geplanter Abweichender Struktur, mit Betreuer(n) absprechen.

___
### Struktur

0. Abstract

    Ein Abstract für das Paper, welches in 200 oder weniger Wörtern das Problem, den Ansatz und das Ergebnis darstellt und zusammengefasst.

1. Introduction

    Eine kurze Problemstellung und Beschreibung der für die Seminararbeit genutzten Daten.

2. Methode

    Darstellung des Designs und Architecture der genutzten Neuronalen Netze. Beschreibung der Regularisierung, Validierung etc.

3. Ergebnisse

    Darstellung der Ergebnisse und Erkenntnisse aus Anwendung

4. Fazit

    Zusammenfassung, sowie kritischer Standpunkt gegenüber eigener Methode.

5. Anhang

___

## Code

Inhaltliche und Style Vorgaben an den einzureichenden Code

- Stringent kommentierter Code
- Eindeutige und geordnete Struktur 
- Jeglicher Code (Python und Latex) ist auf Gitlab hochzuladen
- Code muss durchlaufen

Jeglicher Punkt wird ***kontrolliert!***

## Presentation

Präsentationsvorgaben sein mit dem Betreuer zu besprechen. Eine Latex Vorlage sei im Ordner Templates zu finden, deren Nutzung ist allerdings nicht verpflichtend.

