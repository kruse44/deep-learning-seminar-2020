# Templates

## Paper

In diesem Ordner findet ihr die Vorlage welche ihr zur Erstellung des Papers nutzen sollt. Hierbei findet ihr ein Beispielspaper vor. Die einzelnen Kapitel sind aufgeteilt in verschiedene chapter .tex files. Wichtig ist vor allem die Formatierungseinstellungen im main.tex file. Der hier unterstellte Kapitelaufbau sei dabei nur ein Beispiel, den genauen Aufbau sollt ihr hierbei bitte mit eurem Betreuer absprechen.

In den .tex files werdet ihr Beispiele für die von uns gewünschte Art der Zitierung, Tabellen Layouts, Pseudo-Codes Darstellung, sowie die von uns gewünschte Einbindung von Grafiken. Diese Formatvorlagen sein hierbei bitte wenn möglich einzuhalten.

## Presentation

Diese Ordner beinhaltet eine Latex Vorlage welche ihr für die Erstellung eurer Präsentation nutzen könnt, aber nicht müsst.