# DeepLearning2020


## Beschreibung des Moduls

**Please note:** If you are planning to take this course, **you need to
preregister on Stud.IP before XXXX.** Follow this link to sign up:
<https://studip.uni-goettingen.de/xxxx>.

### Ansprechpartner

Bei Fragen oder Problemen E-Mails bitte an: <rene-marcel.kruse@uni-goettingen.de>


## Important dates


  - Monday, 26. October 2020, 19:00 - 20:00h, **Kick-off meeting** , https://meet.gwdg.de/b/ren-jeh-3mq
  - SomeDay, xx. November 2020, **Deadline** for selection of seminar topic
  - SomeDay, xx. January 2021, 10:00-12:00h, **Presentations**, https://meet.gwdg.de/b/ren-jeh-3mq, 
  - SomeDay: xx. March 2021, 23.59h, **Submission** deadline.


## Technical & statistical prerequisites

### Grundlagen Statistik

Die Teilnehmer des Seminars benötigen **<ins>keine</ins>** besonderen Kenntnisse aus anderen Kursen des Masters "Angewandte Statistik" oder vergleichbarer Studiengängen. Es wäre allerdings hilfreich, wenn die Teilnehmer ein Grundverständnis einer oder mehrerer der folgenden Bereiche aufweisen:   
  - Lineare Modelle, 
  - Multivariate Verfahren,
  - Optimierungsmethoden,
  - Machine/Statistical Learning 

### Grundlagen Programmiersprachen

Das Beherrschen einer Programmiersprache ist nicht verpflichtend. Grundlegendes Wissen und Fähigkeiten in einer der gängigen (statistischen) Programmiersprache wie **R** oder **Python** wäre allerdings wünschenswert.

Große Teile der zu erbringenden Leistung müssen mit Hilfe von Programmiersprachen (**Python**) und Deep Learning Libraries (**Tensorflow**, **Pytorch**,…) erbracht werden. Weswegen Teilnehmer angehalten sind, sich spätestens während des Seminars Grundlegende Kenntnisse zuzulegen.

### Version control using git & GitLab

**Please note:** To start working on your project, log in to GitLab with
your student account once and send me an email. After that, I can add
you to the group and the project.

GitLab is an online platform where you can host git repositories, very
similar to GitHub, and git is a popular version control system for
software development. Systematic, version-controlled software
development is an essential component of this course.

**Please note:** Your code will only be visible to me, the project
supervisors, and your fellow students. Feel free to make mistakes and
ask questions!


## Ablauf des Moduls

### Vorlesungsteil

Der erste Teil des Seminars wird in Form einer Vorlesung gestaltet. Hierbei werden in den ersten Vorlesungswochen die Grundlage und Theorie von Deep Learning Methoden, sowie grundlegender Techniken erklärt.

Auf Grund der aktuellen Situation wird der Vorlesungsteil in Form von Videos abgehalten. Ihr könnt die jeweiligen Videos in Stud.IP oder im Unterordner Videos finden.

Der Inhalt der Vorlesungen bildet das Grundgerüst der Veranstaltung und vermittelt nötiges Verständnis der Materie damit sich die Studenten im Laufe des Seminars tiefergehend mit der Thematik und ihrer Aufgabenstellungen vertrauen können.
  - ...
  - Videos


### Forschungsteil

Betreuer ...
  - [René-Marcel Kruse](http://www.uni-goettingen.de/de/610058.html "René-M. Kruse"), 
  - …
  - …

## Student projects

Die zu nutzende Programmiersprachen ist offen, die Betreuer unterstützen verschiedene Programmiersprachen.

### 1. 

XXX

### 2.

xxx

## Requirements to pass the course

To get the credits for this module, you need to...

  - ... 
  - ...

...

## Struktur dieses Repos sei wie folgt:
 
 1. Templates

    Dieser Ordner beinhaltet jegliche benötigten Latex und Bibliografie Vorlagen, sowie die von uns gewünschten Art und Weisen zu zitieren, Grafiken einzubinden, Tabellen Layout und Pseudo-Code Stil.  

 2. Misc

    Ordner enthält die Formellen Vorgaben zur Seminararbeit und dem Vortrag. Desweiteren sind Angeben wie man richtig mit Git(lab) um zu gehen hat dort zu finden.

 3. Gruppen Ordner
    
    Jede Gruppe erhält einen eigenen Ordner in welchem die Teilnehmer ihren Python sowie Latex Code hochzuladen haben. Desweiteren sei durch die Studenten das README ihres Projektordners selbst zu pflegen und aufzubauen.


## Readings

  - \[1\] “Statistik für Dummies von Dummies” by René-M. Kruse
  - \[1\] “Deep Learning with Python” by Chollet
  - \[2\] “Learning TensorFlow” by Hope et al.
  - \[3\] “Thoughtful machine learning with Python” by Kirk
  - \[4\] “Deep Learning with R” by Allaire \& Chollet
  - \[5\] “Python for Data Analysis” by Mckinney
  - \[6\] “Learning Python” by Lutz
  - \[7\] “Statistik für Dummies von Dummies” by René-M. Kruse



